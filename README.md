# AC05 - Ambiente de Desenvolvimento e Operação - DevOps

## Proposta da atividade:

Em grupos, concluir a sua própria pipeline para execução de Testes Unitários com o PyUnit, Validação de Segurança com Semgrep, build da imagem Docker em um registry e deploy em um serviço AWS;

## Executando os testes unitários via nuvem

```
$ python -m unittest -v tests/appTest.py

test_http_code (tests.appTest.AppTest.test_http_code) ... ok
test_http_status (tests.appTest.AppTest.test_http_status) ... ok
test_print_ac05_DevOps (tests.appTest.AppTest.test_print_ac05_DevOps) ... ok

----------------------------------------------------------------------

Ran 3 tests in 0.005s
OK
```

## Grupo:

Aluno: Amália Emilia da Rocha Pitta  RA: 2200519<br>
Aluno: Beatriz Borges Cantero  RA: 2202122<br>
Aluno: Guilherme Midea Paoliello Castilho  RA: 2200770<br>
Aluno: Gustavo Holanda Soares Santana  RA: 2200545 
<br><br>